# My project's README

Project Spa is an application created by Dan Carver and Ewen Noble
as part of the CIS2232 final project deliverable. 
To run the application please make sure to run the provided sql file.
This application allows you to "book" a future court date based on customer
input, the courts are separated in 3 types (squash, raquetball, tennis) and allows
the admin to do full CRUD on all courts and business hours. 
