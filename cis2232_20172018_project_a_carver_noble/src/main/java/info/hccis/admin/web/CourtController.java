/*
 * Author: Dan Carver
 * Date Created: Dec 06 2017
 * Purpose: Controls all court related CRUD
 */
package info.hccis.admin.web;

import info.hccis.admin.model.jpa.Court;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.model.jpa.User;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import sun.security.x509.AuthorityInfoAccessExtension;

/**
 *
 * @author Dan Carver
 */
@Controller
public class CourtController {

    private final CourtRepository cr;
    private final UserRepository ur;
    private final CodeValueRepository cvr;

    @Autowired
    public CourtController(CourtRepository cr, UserRepository ur, CodeValueRepository cvr) {
        this.cr = cr;
        this.ur = ur;
        this.cvr = cvr;
    }

    @RequestMapping("/courts/add")
    public String add(Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }

        Court court = new Court();
        model.addAttribute("court", court);

        return "courts/add";
    }

    @RequestMapping("/courts/delete")
    public String delete(Model model, HttpSession session, HttpServletRequest request) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }

        String id = request.getParameter("id");
        //CamperDAO.delete(Integer.parseInt(id));
        cr.delete(Integer.parseInt(id));

        ArrayList<CodeValue> codeValues = (ArrayList<CodeValue>) cvr.findAll();
        model.addAttribute("values", codeValues);
        ArrayList<Court> courts = (ArrayList<Court>) cr.findAll(); //CamperDAO.selectAll();
        model.addAttribute("courts", courts);
        return "courts/list";
    }

    @RequestMapping("/courts/edit")
    public String edit(Model model, HttpSession session, HttpServletRequest request) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }

        String id = request.getParameter("id");
        Court court = cr.findOne(Integer.parseInt(id));
        model.addAttribute("court", court);
        session.setAttribute("createdDate", court);

        return "courts/edit";
    }

    @RequestMapping("/courts/addSubmit")
    public String addSubmit(@Valid @ModelAttribute("court") Court court, BindingResult result, Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }
        boolean error = false;

        //Apply Spring Validation
        //If there are errors on the form then send the user back to the add page.
        if (result.hasErrors()) {
            System.out.println("BJM-There was an error validating the court object");
            error = true;
        }

        if (court.getCourtType() < 3 || court.getCourtType() > 5) {
            model.addAttribute("message", "code types are between 3 and 5");
            return "courts/add";
        }

        //ArrayList<String> errors = CourtValidationBO.validateCourt(court);
        //If there is an error send them back to add page.
//        if (!errors.isEmpty()) {
//            error = true;
//        }
//        if (error) {
//            model.addAttribute("messages", errors);
//            return "/court/add";
//        }
        //https://stackoverflow.com/questions/530012/how-to-convert-java-util-date-to-java-sql-date
        java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());

        ArrayList<User> users = (ArrayList<User>) ur.findByUsername(user.getUsername());
        User temp = new User();

        for (int i = 0; i < users.size(); i++) {
            if (user.getUsername().equals(users.get(i).getUsername())) {
                temp = users.get(i);
            }
        }

        if (court.getCreatedUserId() == null) {
            if (temp.getUserTypeCode() == 1) {
                court.setCreatedUserId("ADMIN");
            } else {
                court.setCreatedUserId("GENERAL");
            }
            court.setCreatedDateTime(sqlDate);
        }

        System.out.println("BJM - About to add " + court + " to the database");
        try {
            cr.save(court);
            //CourtDAO.update(court);
            //Get the courts from the database
            ArrayList<CodeValue> codeValues = (ArrayList<CodeValue>) cvr.findAll();
            model.addAttribute("values", codeValues);
            ArrayList<Court> courts = (ArrayList<Court>) cr.findAll();
            model.addAttribute("courts", courts);
        } catch (Exception ex) {
            System.out.println("BJM - There was an error adding court to the database");
        }
        return "courts/list";
    }

    @RequestMapping("/courts/editSubmit")
    public String editSubmit(@Valid @ModelAttribute("court") Court court, BindingResult result, Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }
        boolean error = false;

        //Apply Spring Validation
        //If there are errors on the form then send the user back to the add page.
        if (result.hasErrors()) {
            System.out.println("BJM-There was an error validating the court object");
            error = true;
        }
        
        java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
        
        Court courtTemp = (Court) session.getAttribute("createdDate");
        
        court.setCreatedDateTime(courtTemp.getCreatedDateTime());

        if (court.getCourtType() < 3 || court.getCourtType() > 5) {
            model.addAttribute("message", "code types are between 3 and 5");
            return "courts/edit";
        }

        //ArrayList<String> errors = CourtValidationBO.validateCourt(court);
        //If there is an error send them back to add page.
//        if (!errors.isEmpty()) {
//            error = true;
//        }
//        if (error) {
//            model.addAttribute("messages", errors);
//            return "/court/add";
//        }
        ArrayList<User> users = (ArrayList<User>) ur.findByUsername(user.getUsername());
        User temp = new User();

        for (int i = 0; i < users.size(); i++) {
            if (user.getUsername().equals(users.get(i).getUsername())) {
                temp = users.get(i);
            }
        }

        if (temp.getUserTypeCode() == 1) {
            court.setUpdatedUserId("ADMIN");
        } else {
            court.setUpdatedUserId("GENERAL");
        }
        court.setUpdatedDateTime(sqlDate);

        System.out.println("BJM - About to add " + court + " to the database");
        try {
            cr.save(court);
            //CourtDAO.update(court);
            //Get the courts from the database
            ArrayList<CodeValue> codeValues = (ArrayList<CodeValue>) cvr.findAll();
            model.addAttribute("values", codeValues);
            ArrayList<Court> courts = (ArrayList<Court>) cr.findAll();
            model.addAttribute("courts", courts);
        } catch (Exception ex) {
            System.out.println("BJM - There was an error adding court to the database");
        }
        return "courts/list";
    }

    @RequestMapping("/courts/list")
    public String showHome(Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }

        ArrayList<CodeValue> codeValues = (ArrayList<CodeValue>) cvr.findAll();
//        ArrayList<String> descriptions = new ArrayList<String>();

        //Get the courts from the database
        ArrayList<Court> courts = (ArrayList<Court>) cr.findAll();
        System.out.println("BJM-found " + courts.size() + " courts.  Going to welcome page");

//        for (int i = 0; i < courts.size(); i++)
//        {
//            for (int ii = 0; ii < codeValues.size(); ii++)
//            {
//                if (courts.get(i).getCourtType() == codeValues.get(ii).getCodeValueSequence())
//                {
//                    descriptions.add(codeValues.get(ii).getEnglishDescription());
//                }
//            }
//        }
        model.addAttribute("values", codeValues);
        model.addAttribute("courts", courts);

        //This will send the user to the welcome.html page.
        return "courts/list";
    }

}
