/*
*Author: Ewen Noble
*Since: 12/3/17
*Purpose: This is a rest web service for the Booking Service, this allows
 user to view all available courts with start times.
 */
package info.hccis.admin.web.services;

//import com.google.gson.Gson;
//import info.hccis.player.data.springdatajpa.PlayerRepository;
//import info.hccis.player.model.jpa.Player;
import com.google.gson.Gson;
import info.hccis.admin.dao.BookingDAO;
import info.hccis.admin.data.springdatajpa.BookingRepository;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.data.springdatajpa.ScheduleRepository;
import info.hccis.admin.model.jpa.Booking;
import info.hccis.admin.model.jpa.Court;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

//issues here...
//http://www.scriptscoop.net/t/ba4553fde8ca/spring-autowired-classes-are-null-in-jersey-rest.html

@Component
@Path("/BookingServiceJpa")
public class BookingServiceJpa {

    @Resource
    private final BookingRepository br;
    private final CourtRepository cr;
    private final ScheduleRepository sr;

    @Autowired
    public BookingServiceJpa(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.br = applicationContext.getBean(BookingRepository.class);
        this.cr = applicationContext.getBean(CourtRepository.class);
        this.sr = applicationContext.getBean(ScheduleRepository.class);
    }


 

    /*
    Author: Ewen Noble
    Since: 12/5/17
    Purpose: This is to provide JSON string to show available bookings. 
    Right now it shows the userID even though I have it commented out. 
     */
    @GET
    @Path("/bookings")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBookings() {

        //Get the players from the database
        //ArrayList<Booking> bookings = (ArrayList) br.findAll();
        Booking booking = new Booking();
        ArrayList<Booking> bookings = BookingDAO.getFutureTimes(booking);
        System.out.println("EN Finding available bookings --- " + bookings);

        Gson gson = new Gson();

        int statusCode = 200;
        if (bookings.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(bookings);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
      @GET
    @Path("/bookings/{courtType}/{date}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBookingsByTypeAndDate(@PathParam("courtType")int type, @PathParam("bookingDate") String date) {

       ArrayList<Court> courts = (ArrayList<Court>) cr.findAll();
       ArrayList<Booking> bookings = (ArrayList<Booking>) br.findByBookingDate(date);
       for(Court tempCourt : courts){
           for (Booking tempBooking : bookings){
               if(tempCourt.getCourtType()==type){
               if(tempCourt.getCourtId()!= tempBooking.getCourtId()){
                   Gson gson = new Gson();
                    String temp = "";
        temp = gson.toJson(courts);
               }
           }
           }
       }
        //Gson gson = new Gson();

        int statusCode = 200;
        if (courts.isEmpty()) {
            statusCode = 204;
        }
                        Gson gson = new Gson();
                    String temp = "";
        temp = gson.toJson(courts);

//        String temp = "";
//        temp = gson.toJson(courts);

        return Response.status(statusCode).entity(courts).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

    @GET
    @Path("/bookings/{id}/{startDate}/{endDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id, @PathParam("startDate") String startDate, @PathParam("endDate") String endDate) {

       
        
        //Get the players from the database
        //ArrayList<Booking> bookings = (ArrayList) br.findAll();
        Booking booking = new Booking();
        ArrayList<Booking> bookings = BookingDAO.getSpecificBookings(booking, id, startDate, endDate);
        System.out.println("EN Finding available bookings --- " + bookings);

        Gson gson = new Gson();

        int statusCode = 200;
        if (bookings.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(bookings);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
    @GET
    @Path("/bookings/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id) {

        //http://localhost:8080/courtbooking/rest/BookingServiceJpa/bookings/1/2016-01-01/2017-11-13
        
        //Get the players from the database
        //ArrayList<Booking> bookings = (ArrayList) br.findAll();
        Booking booking = new Booking();
        ArrayList<Booking> bookings = BookingDAO.getSpecificBookingsById(booking, id);
        System.out.println("EN Finding available bookings --- " + bookings);

        Gson gson = new Gson();

        int statusCode = 200;
        if (bookings.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(bookings);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
    
}
