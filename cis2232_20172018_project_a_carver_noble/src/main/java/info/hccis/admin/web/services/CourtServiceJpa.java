/*
*Author: Ewen Noble
*Since: 12/3/17
*Purpose: This is a rest web service for the Booking Service, this allows
 user to view all available courts with start times.
 */
package info.hccis.admin.web.services;

//import com.google.gson.Gson;
//import info.hccis.player.data.springdatajpa.PlayerRepository;
//import info.hccis.player.model.jpa.Player;
import com.google.gson.Gson;
import info.hccis.admin.dao.BookingDAO;
import info.hccis.admin.data.springdatajpa.BookingRepository;
import info.hccis.admin.model.jpa.Booking;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.model.jpa.Court;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

//issues here...
//http://www.scriptscoop.net/t/ba4553fde8ca/spring-autowired-classes-are-null-in-jersey-rest.html
@Component
@Path("/CourtServiceJpa")
public class CourtServiceJpa {

    @Resource
    private final BookingRepository br;
    private final CourtRepository cr;

    public CourtServiceJpa(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.br = applicationContext.getBean(BookingRepository.class);
        this.cr = applicationContext.getBean(CourtRepository.class);
    }

//    @Autowired
    public CourtServiceJpa() {
        br = null;
        cr = null;
    }

    /*
    Author: Ewen Noble
    Since: 12/5/17
    Purpose: This is to provide JSON string to show available bookings. 
    Right now it shows the userID even though I have it commented out. 
     */
    @GET
    @Path("/courts")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCourts() {

        //Get the players from the database
        //ArrayList<Booking> bookings = (ArrayList) br.findAll();
        //Booking booking = new Booking();
        ArrayList<Booking> bookings = (ArrayList<Booking>) br.findAll();
        ArrayList<Court> courts = (ArrayList<Court>) cr.findAll();
        //ArrayList<Booking> bookings = BookingDAO.getFutureTimes(booking);
        System.out.println("EN Finding available bookings --- " + bookings);

        Gson gson = new Gson();

        int statusCode = 200;
        if (bookings.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        
//        int i = 0;
        
        for (int i = 0; i < courts.size(); i++)
        {
            for (int ii = 0; i < bookings.size(); ii++)
            {
                if (bookings.get(i).getCourtId() == courts.get(ii).getCourtId())
                {
                temp = gson.toJson(courts.get(i));                    
                }
            }
        }

        
        //temp = gson.toJson(bookings);

        return Response.status(statusCode).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
}
