/*
 * Author: Ewen Noble
 * Date Created: Nov 22 2017
 * Purpose: Controls requests from Search, Results and other booking requests. 
 * Uses BookingRepository to do SQL
 */
package info.hccis.admin.web;

import info.hccis.admin.dao.BookingDAO;
import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.dao.UserAccessDAO;
import info.hccis.admin.data.springdatajpa.BookingRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.Booking;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.model.jpa.User;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Ewen
 */
@Controller
public class BookingController {

    private final BookingRepository br;
    //adding user repository to do report --- EN -- 12/8/17
    private final UserRepository ur;

    @Autowired
    public BookingController(BookingRepository br, UserRepository ur) {
        this.br = br;
        this.ur = ur;
    }

    /*
    * Author Ewen Noble
    * Purpose: This is the search page for the bookings. It populates a list of 
    * "opponents" that are all the users from the database. It also populates a list
    * of court types based off of code values stored in database. It also has a 
    * HTML 5 date picker to allow them to select their booking date.
    */
    @RequestMapping("/booking/search")
    public String authenticate(Model model, @ModelAttribute("user") User user, HttpSession session) {
        
        Booking booking = new Booking();
        User userString = (User) session.getAttribute("loggedInUser");
        //ArrayList<User> users = (ArrayList<User>) ur.findByUsername(user.getUsername());
        int uId = userString.getUserId();

        System.out.println(uId);
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        ArrayList<User> users = UserAccessDAO.getUsers(databaseConnection);

        model.addAttribute(booking);
        session.setAttribute("users", users);

        ArrayList<CodeValue> courtTypes = CodeValueDAO.getCodeValues(new DatabaseConnection(), "2");
        //   System.out.println("BJM - added "+campTypes.size()+" to the session for camp types");
        session.setAttribute("courtTypes", courtTypes);
          ArrayList<Booking> bookingsResults = (ArrayList<Booking>) br.findByUserId(uId);
         int size = bookingsResults.size();
         if(size >= 2){
             model.addAttribute("message", "Can only have 2 future bookings");
         }

        return "/booking/search";
    }

    /*
    * Author: Ewen Noble
    * Purpose: When the user clicks SUBMIT from search.html this request mapping
    * kicks in. It loads an arraylist of available bookings based off their requirements.
    * In this controller the userID, booking date and opponent is SET.
    */
    @RequestMapping("/booking/submitSearch")
    public String addSubmit(Model model, HttpSession session, @ModelAttribute("bookings") Booking booking) {
        System.out.println("EN -- ARE YOU GETTING HERE?");
        User userString = (User) session.getAttribute("loggedInUser");

        ArrayList<Booking> bookings = BookingDAO.getSearchResults(booking);

        System.out.println("EN Here is starting dates " + booking.getBookingDate());

        int uId = userString.getUserId();

        System.out.println("USER ID ---" + userString.getUserId());
        System.out.println("USERNAME --- " + userString.getUsername());
        System.out.println("PASSword ---" + userString.getPassword());
        System.out.println("TYPE ---" + userString.getUserTypeDescription());
        for (Booking temp : bookings) {
            temp.setBookingDate(booking.getBookingDate());
            temp.setUserId(userString.getUserId());
            temp.setOpponent(booking.getOpponent());
        }
        System.out.println("BOOKING USER ID ---" + booking.getUserId());
        System.out.println("BOOKING OPPONENT ---" + booking.getOpponent());

        System.out.println("EN Here is court type " + booking.getCourtType());
       

        model.addAttribute("bookings", bookings);

        return "/booking/results";
    }

    /*
     * Author Ewen Noble
     * Purpose: After the user chooses their booking from results.html this conroller
     * kicks in. It does a final check to make sure no one else booked the same book
     * at the same time. I set the userID one more time here because it wasn't making it all 
     * way. The booking is then saved to the database using the repository. A auditing
     * file is created and the booking details are written to the file, the file is saved
     * to root drive under "courtbookings".
    */
    @RequestMapping("/booking/bookingConfirmation")
    public String bookingConfirmation(Model model, @ModelAttribute("user") User user, HttpSession session, Booking booking) {

        User userString = (User) session.getAttribute("loggedInUser");
        String uName = userString.getUsername();
        ArrayList<Booking> allBookings = (ArrayList<Booking>) br.findAll();
//        int check = booking.getCourtId();
//       
//        if(booking.getCourtId() == br.findByCourtId(check)){
//            
//        } else {
//        }
        for(Booking temp : allBookings){
            if(temp.getCourtId() == booking.getCourtId()){
                 model.addAttribute("message", "Booking already taken");
            }
        }

//         br.findOne(booking.getUserId());

        int uId = (int) userString.getUserId();
//        System.out.println(uId);
//       User userString = new User();
//       userString.setUsername(session.getAttribute("loggedInUser"));
        System.out.println("EN -- STARTING BOOKING");

        booking.setUserId(uId);
        br.save(booking);
        model.addAttribute(booking);
        //write information to file - method taking from class example EN
        String FILE_NAME = "/cis2232/courtbookings/booking.out";
        try {
            //Create a file
            Files.createDirectories(Paths.get("/cis2232/courtbookings"));
        } catch (IOException ex) {
            System.out.println("io exception caught");
        }

        //Write date booked/booking details to file
        BufferedWriter bw = null;
        FileWriter fw = null;
        //booking string taken from mykong
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        
//System.out.println(dtf.format(now));

        try {
            fw = new FileWriter(FILE_NAME, true);
            bw = new BufferedWriter(fw);
            bw.write((dtf.format(now)) + " " + uName + "  " + "courtId=" + booking.getCourtId() + " " + booking.getBookingDate() + " " + booking.getBookingStartTime() + "\r\n");
//            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return "/booking/bookingResults";
    }

    /*
     * This controller kicks in whenever the user selects view all bookings (myBookings.html).
     * An arraylist of bookings is made based off logged in user and sent to the view.
    */
    @RequestMapping("/booking/myBookings")
    public String myBookings(Model model, HttpSession session, Booking booking) {
        User userString = (User) session.getAttribute("loggedInUser");
        int uId = userString.getUserId();

        ArrayList<Booking> bookings = (ArrayList<Booking>) br.findByUserId(uId);

        model.addAttribute("bookings", bookings);

        return "/booking/myBookings";
    }

}
