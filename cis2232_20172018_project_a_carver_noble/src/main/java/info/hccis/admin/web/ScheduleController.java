/*
 * Author: Dan Carver
 * Date Created: Dec 06 2017
 * Purpose: Controls all schedule related CRUD
 */
package info.hccis.admin.web;

import info.hccis.admin.model.jpa.Schedule;
import info.hccis.admin.data.springdatajpa.ScheduleRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.model.jpa.User;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Dan Carver
 */
@Controller
public class ScheduleController {

    private final ScheduleRepository sr;
    private final UserRepository ur;

    @Autowired
    public ScheduleController(ScheduleRepository sr, UserRepository ur) {
        this.sr = sr;
        this.ur = ur;
    }

    @RequestMapping("/schedule/add")
    public String add(Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }

        Schedule schedule = new Schedule();
        model.addAttribute("schedules", schedule);

        return "schedule/add";
    }

    @RequestMapping("/schedule/delete")
    public String delete(Model model, HttpSession session, HttpServletRequest request) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }

        String id = request.getParameter("id");
        //CamperDAO.delete(Integer.parseInt(id));
        sr.delete(Integer.parseInt(id));

        ArrayList<Schedule> schedule = (ArrayList<Schedule>) sr.findAll(); //CamperDAO.selectAll();
        model.addAttribute("schedules", schedule);
        return "schedule/list";
    }

    @RequestMapping("/schedule/edit")
    public String edit(Model model, HttpSession session, HttpServletRequest request) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }

        String id = request.getParameter("id");
        Schedule schedule = sr.findOne(Integer.parseInt(id));
        model.addAttribute("schedules", schedule);
        session.setAttribute("createdDate", schedule);

        return "schedule/edit";
    }

    @RequestMapping("/schedule/addSubmit")
    public String addSubmit(@Valid @ModelAttribute("schedule") Schedule schedule, BindingResult result, Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }
        boolean error = false;

        //Apply Spring Validation
        //If there are errors on the form then send the user back to the add page.
        if (result.hasErrors()) {
            System.out.println("BJM-There was an error validating the schedule object");
            error = true;
        }

        //ArrayList<String> errors = ScheduleValidationBO.validateSchedule(schedule);
        //If there is an error send them back to add page.
//        if (!errors.isEmpty()) {
//            error = true;
//        }
//        if (error) {
//            model.addAttribute("messages", errors);
//            return "/schedule/add";
//        }
        System.out.println("BJM - About to add " + schedule + " to the database");
        try {

            //https://stackoverflow.com/questions/530012/how-to-convert-java-util-date-to-java-sql-date
            java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());

            ArrayList<User> users = (ArrayList<User>) ur.findByUsername(user.getUsername());
            User temp = new User();

            for (int i = 0; i < users.size(); i++) {
                if (user.getUsername().equals(users.get(i).getUsername())) {
                    temp = users.get(i);
                }
            }

            if (schedule.getCreatedUserId() == null) {
                if (temp.getUserTypeCode() == 1) {
                    schedule.setCreatedUserId("ADMIN");
                } else {
                    schedule.setCreatedUserId("GENERAL");
                }
                schedule.setCreatedDateTime(sqlDate);
            }

            sr.save(schedule);
            //ScheduleDAO.update(schedule);
            //Get the schedule from the database
            ArrayList<Schedule> schedules = (ArrayList<Schedule>) sr.findAll();
            model.addAttribute("schedules", schedules);
        } catch (Exception ex) {
            System.out.println("BJM - There was an error adding schedule to the database");
        }
        return "schedule/list";
    }

    @RequestMapping("/schedule/editSubmit")
    public String editSubmit(@Valid @ModelAttribute("schedule") Schedule schedule, BindingResult result, Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }
        boolean error = false;

        //Apply Spring Validation
        //If there are errors on the form then send the user back to the add page.
        if (result.hasErrors()) {
            System.out.println("BJM-There was an error validating the schedule object");
            error = true;
        }
        
        Schedule scheduleTemp = (Schedule) session.getAttribute("createdDate");
        
//        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
//        Date parsedDate = formatter.parse(scheduleTemp.getCreatedDateTime());

          schedule.setCreatedDateTime(scheduleTemp.getCreatedDateTime());
          
//        if (schedule.getStartTime().equals("") || schedule.getEndTime().equals(""))
//        {
//            model.addAttribute("message", "must enter times");
//            return "schedule/edit";
//        }
        //ArrayList<String> errors = ScheduleValidationBO.validateSchedule(schedule);
        //If there is an error send them back to add page.
//        if (!errors.isEmpty()) {
//            error = true;
//        }
//        if (error) {
//            model.addAttribute("messages", errors);
//            return "/schedule/add";
//        }
        System.out.println("BJM - About to add " + schedule + " to the database");
        try {

            java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());

            ArrayList<User> users = (ArrayList<User>) ur.findByUsername(user.getUsername());
            User temp = new User();

            for (int i = 0; i < users.size(); i++) {
                if (user.getUsername().equals(users.get(i).getUsername())) {
                    temp = users.get(i);
                }
            }

            if (temp.getUserTypeCode() == 1) {
                schedule.setUpdatedUserId("ADMIN");
            } else {
                schedule.setUpdatedUserId("GENERAL");
            }

            schedule.setUpdatedDateTime(sqlDate);

            sr.save(schedule);
            //ScheduleDAO.update(schedule);
            //Get the schedule from the database
            ArrayList<Schedule> schedules = (ArrayList<Schedule>) sr.findAll();
            model.addAttribute("schedules", schedules);
        } catch (Exception ex) {
            System.out.println("BJM - There was an error adding schedule to the database");
        }
        return "schedule/list";
    }

    @RequestMapping("/schedule/list")
    public String showHome(Model model, HttpSession session) {

        //make sure logged in
        User user = (User) session.getAttribute("loggedInUser");
        if (user == null) {
            model.addAttribute("user", new User());
            return "other/databaseInformation";
        }
        
        //Get the schedule from the database
        ArrayList<Schedule> schedules = (ArrayList<Schedule>) sr.findAll();
        System.out.println("BJM-found " + schedules.size() + " schedule.  Going to welcome page");
        model.addAttribute("schedules", schedules);

        //This will send the user to the welcome.html page.
        return "schedule/list";
    }

}
