package info.hccis.admin.web;

import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.data.springdatajpa.UserRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.model.jpa.Court;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.util.Utility;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {
    
    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;
    private final CourtRepository cr;
    private final UserRepository ur;
    
    @Autowired
    public OtherController(CodeTypeRepository ctr, CodeValueRepository cvr, CourtRepository cr, UserRepository ur) {
        this.ctr = ctr;
        this.cvr = cvr;
        this.cr = cr;
        this.ur = ur;
    }
    
    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }
    
    @RequestMapping("/other/about")
    public String showAbout(Model model) {
        // System.out.println("Count from code_type="+ctr.count());

//        ArrayList<CodeType> cts = (ArrayList<CodeType>) ctr.findByEnglishDescription("User Types");
//        System.out.println("BJM-"+cts);
//        
//        ArrayList<CodeType> codeTypesByBob = (ArrayList<CodeType>) ctr.findByCreatedUserId("Bob");
//     
//        ArrayList<CodeValue> codeValues = (ArrayList<CodeValue>) cvr.findAll();
//        System.out.println("Here are the Code values");     
//        for(CodeValue thisOne: codeValues){
//            System.out.println(thisOne);
//        }
//        System.out.println("Here are the Code types created by bob");     
//        for(CodeType thisOne: codeTypesByBob){
//            System.out.println(thisOne);
//        }
//        
//        //Create a new code value here.
//        CodeValue newCodeValue = new CodeValue(4,1,"TestNew","TN2");
//        cvr.save(newCodeValue);
        return "other/about";
    }
    
    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }
    
    @RequestMapping("/")
    public String showHome(Model model, HttpSession session) {
        System.out.println("in controller for /");
        //setup the databaseConnection object in the model.  This will be used on the 
        //view.
        model.addAttribute("user", new User());
        session.removeAttribute("loggedInUser");
        System.out.println("MD5 hash for 123=" + Utility.getHashPassword("123"));
        DatabaseConnection databaseConnection = new DatabaseConnection();
        model.addAttribute("databaseConnection", databaseConnection);
        
        return "other/databaseInformation";
    }
    
    @RequestMapping("/logout")
    public String logout(Model model, HttpSession session) {
        
        model.addAttribute("user", new User());
        
        if (!session.getAttribute("loggedInUser").equals(null)) {
            session.removeAttribute("loggedInUser");
            //Give a message indicating that they have been logged out.
            model.addAttribute("message", "Successfully logged out");
            //This will send the user to the welcome.html page.
            return "other/databaseInformation";
        }
        else
        {
            model.addAttribute("message", "You are not logged in");
            //This will send the user to the welcome.html page.
            return "other/databaseInformation";
        }
        
    }
    
    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") User user, HttpSession session) throws Exception {
//        Calendar date = new GregorianCalendar();
//        int month = date.get(Calendar.MONTH);
//        int year = date.get(Calendar.YEAR);
//        int day = date.get(Calendar.DAY_OF_MONTH);
//        int seconds = date.get(Calendar.SECOND);
//        int minutes = date.get(Calendar.MINUTE);
//        int hours = date.get(Calendar.HOUR);
//        String dt = year + "-" + (month + 1) + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
        
        String write = "";
        
        ArrayList<User> usersFromDB = (ArrayList<User>) ur.findByUsername(user.getUsername());
        boolean authenticated = true;
        boolean authenticatedUsername = true;
        //if they exist verify that the password hash matches the hashed pw in the db
        String hashedEnteredPassword = Utility.getMD5Hash(user.getPassword());
        
        session.removeAttribute("loggedInUser");
        
        if (user.getUsername().isEmpty()) {
            //failed validation
            model.addAttribute("message", "Put something in username");
            write = Utility.getNow("yyyy-MM-dd HH:mm:ss") + " " + user.getUsername() + " false";
            writeToFile(write + "\r\n");
            return "other/databaseInformation";
        } else if (user.getPassword().isEmpty()) {
            model.addAttribute("message", "Put something in password");
            write = Utility.getNow("yyyy-MM-dd HH:mm:ss") + " " + user.getUsername() + " false";
            writeToFile(write + "\r\n");
            return "other/databaseInformation";
        } else if (usersFromDB.size() != 1) {
            authenticatedUsername = false;
            model.addAttribute("message", "user does not exist");
            write = Utility.getNow("yyyy-MM-dd HH:mm:ss") + " " + user.getUsername() + " false";
            writeToFile(write + "\r\n");
            return "other/databaseInformation";
        } else if ((user.getPassword().equals(usersFromDB.get(0).getPassword()))
                || (usersFromDB.get(0).getPassword().equals(hashedEnteredPassword))) {
            authenticated = true;
        } else {
            authenticated = false;
            model.addAttribute("message", "password does not match");
            write = Utility.getNow("yyyy-MM-dd HH:mm:ss") + " " + user.getUsername() + " false";
            writeToFile(write + "\r\n");
            return "other/databaseInformation";
        }
        
        if (authenticated && authenticatedUsername) {
            //passed and send to the list page (camper/list)
            //Get the campers from the database
            user = usersFromDB.get(0);
            session.setAttribute("loggedInUser", user);
            //ArrayList<Camper> campers = CamperDAO.selectAll();
            ArrayList<Court> courts = (ArrayList<Court>) cr.findAll();

            write = Utility.getNow("yyyy-MM-dd HH:mm:ss") + " " + user.getUsername() + " true";
            writeToFile(write + "\r\n");
            
            System.out.println("BJM-found " + courts.size() + " courts.  Going to welcome page");
            model.addAttribute("courts", courts);
        }
//        else {
//            //passed and send to the list page (camper/list)
//            //Get the campers from the database
//            session.setAttribute("loggedInUser", user);
//            //ArrayList<Camper> campers = CamperDAO.selectAll();
//            ArrayList<Court> courts = (ArrayList<Court>) cr.findAll();
//            //Also load teh campTypeDescription
////            for (Court court : courts) {
////                try {
////                    CodeValue theCodeValueForThisGuysCampType = cvr.findOne(court.getCourtType());
////                    court.set(theCodeValueForThisGuysCampType.getEnglishDescription());
////                } catch (Exception e) {
////                    court.setCampTypeDescription("None");
////                }
////            }
//
//            System.out.println("BJM-found " + courts.size() + " courts.  Going to welcome page");
//            model.addAttribute("courts", courts);
//        }

//        if (!UserBO.authenticate(user, ur)) {
//            //failed validation
//            //session.setAttribute("loggedInUser", user);
//            model.addAttribute("message", "Authentication failed");
//            return "other/welcome";
//        } else {
        //passed and send to the list page (camper/list)
        //Get the campers from the database
        //    session.setAttribute("loggedInUser", UserBO.getUserByUsername(user, ur));
        //Load the camper types from CodeValue for use when adding a camper
        ArrayList<CodeValue> courtTypes = CodeValueDAO.getCodeValues(new DatabaseConnection(), "2");
        //   System.out.println("BJM - added "+campTypes.size()+" to the session for camp types");
        session.setAttribute("CourtTypes", courtTypes);
        ArrayList<CodeValue> codeValues = (ArrayList<CodeValue>) cvr.findAll();
        model.addAttribute("values", codeValues);
        //}
        return "courts/list";
    }
    
    //taken from bj maclean
    public static void writeToFile(String textToWrite) {
        String FILE_NAME = "/courtbookings/login.out";
        try {
            //Create a file
            Files.createDirectories(Paths.get("/courtbookings"));
        } catch (IOException ex) {
            System.out.println("io exception caught");
        }

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(FILE_NAME, true);
            bw = new BufferedWriter(fw);
            bw.write(textToWrite);
            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
