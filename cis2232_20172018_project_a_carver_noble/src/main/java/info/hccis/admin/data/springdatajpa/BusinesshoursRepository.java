package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.Businesshours;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinesshoursRepository extends CrudRepository<Businesshours, Integer> {

}