/*
* Author: Ewen Noble
* Since: ----
* Purpose: DAO for Booking searches. Methods include getting booking results
* made from user on search.html. It also has a method for BookingServiceJPA
* to return a JSON string of all available courts and start times.
 */
package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.jpa.Booking;
import info.hccis.admin.model.jpa.CodeType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Ewen
 */
public class BookingDAO {

    public static  ArrayList<Booking> getSearchResults(Booking booking) {

        ArrayList<Booking> availableTimes = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        booking.getCourtType();

        try {
            conn = ConnectionUtils.getConnection();

            // sql = "SELECT * FROM `CodeType` order by codeTypeId";
            sql = "SELECT * FROM court c, schedule where c.courtType=?";
            ps = conn.prepareStatement(sql);

            ps.setInt(1, booking.getCourtType());
          
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getString(2);
                int courtId = rs.getInt("courtId");
                int courtType = rs.getInt("courtType");
//               String createdDateTime=rs.getString("createdDateTime");
//               String createdUserId=rs.getString("createdUserId");
//               String updatedDateTime=rs.getString("updatedDateTime");
//               String updatedUserId=rs.getString("updatedUserId");
                int scheduleId = rs.getInt("scheduleID");
                String startTime = rs.getString("startTime");
                String endTime = rs.getString("endTime");
             
//               String createdDateTime=rs.getString("createdDateTime");
//               String createdUserId=rs.getString("createdUserId");
//               String updatedDateTime=rs.getString("updatedDateTime");
//               String updatedUserId=rs.getString("uupdatedUserId");

                // Booking availableBooking = new Booking(courtId,courtType,createdDateTime,createdUserId,updatedDateTime,updatedUserId,scheduleId,startTime,endTime);
                Booking availableBooking = new Booking();
                availableBooking.setBookingId(0);
                availableBooking.setCourtId(courtId);
                availableBooking.setBookingStartTime(startTime);
                availableBooking.setCourtType(courtType);
                
               
                System.out.println("EN adding a booking ----");
                
                
                //November 30 - setting opponent, user id here for now
                //availableBooking.setOpponent(1);
              //  availableBooking.setUserId(1);
                
//                codeType.setCodeTypeId(rs.getInt("codeTypeId"));
//                codeType.setEnglishDescription(rs.getString("englishDescription"));
//                codeType.setFrenchDescription(rs.getString("frenchDescription"));
//                System.out.println("Found code type=" + codeType);
                availableTimes.add(availableBooking);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return availableTimes;
    }
    public static ArrayList<Booking> getFutureTimes(Booking booking) {

        ArrayList<Booking> availableTimes = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        booking.getCourtType();

        try {
            conn = ConnectionUtils.getConnection();

            // sql = "SELECT * FROM `CodeType` order by codeTypeId";
            sql = "SELECT * FROM court c, schedule ";
            ps = conn.prepareStatement(sql);

           // ps.setInt(1, booking.getCourtType());
          
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getString(2);
                int courtId = rs.getInt("courtId");
                int courtType = rs.getInt("courtType");
//               String createdDateTime=rs.getString("createdDateTime");
//               String createdUserId=rs.getString("createdUserId");
//               String updatedDateTime=rs.getString("updatedDateTime");
//               String updatedUserId=rs.getString("updatedUserId");
                int scheduleId = rs.getInt("scheduleID");
                String startTime = rs.getString("startTime");
                String endTime = rs.getString("endTime");
//               String createdDateTime=rs.getString("createdDateTime");
//               String createdUserId=rs.getString("createdUserId");
//               String updatedDateTime=rs.getString("updatedDateTime");
//               String updatedUserId=rs.getString("uupdatedUserId");

                // Booking availableBooking = new Booking(courtId,courtType,createdDateTime,createdUserId,updatedDateTime,updatedUserId,scheduleId,startTime,endTime);
                Booking availableBooking = new Booking();
                availableBooking.setBookingId(0);
                availableBooking.setCourtId(courtId);
                availableBooking.setBookingStartTime(startTime);
                availableBooking.setCourtType(courtType);
                
                System.out.println("EN adding a booking ----");
                
                
                //November 30 - setting opponent, user id here for now
//                availableBooking.setOpponent(1);
//                availableBooking.setUserId(1);
                
//                codeType.setCodeTypeId(rs.getInt("codeTypeId"));
//                codeType.setEnglishDescription(rs.getString("englishDescription"));
//                codeType.setFrenchDescription(rs.getString("frenchDescription"));
//                System.out.println("Found code type=" + codeType);
                availableTimes.add(availableBooking);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return availableTimes;
    }
    
        public static ArrayList<Booking> getSpecificBookings(Booking booking, int id, String startDate, String endDate) {

        ArrayList<Booking> usersBookings = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        booking.getCourtType();

        try {
            conn = ConnectionUtils.getConnection();

            // sql = "SELECT * FROM `CodeType` order by codeTypeId";
            //SELECT * FROM booking where userId = 1 and bookingDate > '2016-01-01' and bookingDate < '2017-11-13'
            sql = "SELECT * FROM booking where userId = ? and bookingDate > ? and bookingDate < ?";
            ps = conn.prepareStatement(sql);

           // ps.setInt(1, booking.getCourtType());
           ps.setInt(1, id);
           ps.setString(2, startDate);
           ps.setString(3, endDate);
          
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                // It is possible to get the columns via name
                // also possible to get the columns via the column number
                // which starts at 1

                // e.g. resultSet.getString(2);
                int bookingId = rs.getInt("bookingId");
                int userId = rs.getInt("userId");
                int courtId = rs.getInt("courtId");
                String bookingDate = rs.getString("bookingDate");
                String bookingStartTime = rs.getString("bookingStartTime");
                Date createdDateTime = rs.getDate("createdDateTime");
                String createdUserId = rs.getString("createdUserId");
                Date updatedDateTime = rs.getDate("updatedDateTime");
                String updatedUserId = rs.getString("updatedUserId");
//               String createdDateTime=rs.getString("createdDateTime");
//               String createdUserId=rs.getString("createdUserId");
//               String updatedDateTime=rs.getString("updatedDateTime");
//               String updatedUserId=rs.getString("updatedUserId");
//                int scheduleId = rs.getInt("scheduleID");
//                String startTime = rs.getString("startTime");
//                String endTime = rs.getString("endTime");
//               String createdDateTime=rs.getString("createdDateTime");
//               String createdUserId=rs.getString("createdUserId");
//               String updatedDateTime=rs.getString("updatedDateTime");
//               String updatedUserId=rs.getString("uupdatedUserId");

                // Booking availableBooking = new Booking(courtId,courtType,createdDateTime,createdUserId,updatedDateTime,updatedUserId,scheduleId,startTime,endTime);
                Booking availableBooking = new Booking();
                availableBooking.setBookingId(bookingId);
                availableBooking.setUserId(userId);
                availableBooking.setCourtId(courtId);
                availableBooking.setBookingDate(bookingDate);
                availableBooking.setBookingStartTime(bookingStartTime);
                availableBooking.setCreatedDateTime(createdDateTime);
                availableBooking.setCreatedUserId(createdUserId);
                availableBooking.setUpdatedDateTime(updatedDateTime);
                availableBooking.setUpdatedUserId(updatedUserId);
                
                System.out.println("EN adding a booking ----");
                
                
                //November 30 - setting opponent, user id here for now
//                availableBooking.setOpponent(1);
//                availableBooking.setUserId(1);
                
//                codeType.setCodeTypeId(rs.getInt("codeTypeId"));
//                codeType.setEnglishDescription(rs.getString("englishDescription"));
//                codeType.setFrenchDescription(rs.getString("frenchDescription"));
//                System.out.println("Found code type=" + codeType);
                usersBookings.add(availableBooking);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return usersBookings;
    }
        
        public static ArrayList<Booking> getSpecificBookingsById(Booking booking, int id) {

        ArrayList<Booking> usersBookings = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        booking.getCourtType();

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM booking where userId = ?";
            ps = conn.prepareStatement(sql);

           ps.setInt(1, id);
          
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int bookingId = rs.getInt("bookingId");
                int userId = rs.getInt("userId");
                int courtId = rs.getInt("courtId");
                String bookingDate = rs.getString("bookingDate");
                String bookingStartTime = rs.getString("bookingStartTime");
                Date createdDateTime = rs.getDate("createdDateTime");
                String createdUserId = rs.getString("createdUserId");
                Date updatedDateTime = rs.getDate("updatedDateTime");
                String updatedUserId = rs.getString("updatedUserId");
                
                Booking availableBooking = new Booking();
                
                availableBooking.setBookingId(bookingId);
                availableBooking.setUserId(userId);
                availableBooking.setCourtId(courtId);
                availableBooking.setBookingDate(bookingDate);
                availableBooking.setBookingStartTime(bookingStartTime);
                availableBooking.setCreatedDateTime(createdDateTime);
                availableBooking.setCreatedUserId(createdUserId);
                availableBooking.setUpdatedDateTime(updatedDateTime);
                availableBooking.setUpdatedUserId(updatedUserId);
                
                System.out.println("EN adding a booking ----");
                
                usersBookings.add(availableBooking);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return usersBookings;
    }
}
