/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dcarver
 */
@Entity
@Table(name = "businesshours")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Businesshours.findAll", query = "SELECT b FROM Businesshours b")
    , @NamedQuery(name = "Businesshours.findByBusinessHoursId", query = "SELECT b FROM Businesshours b WHERE b.businessHoursId = :businessHoursId")
    , @NamedQuery(name = "Businesshours.findByDayType", query = "SELECT b FROM Businesshours b WHERE b.dayType = :dayType")
    , @NamedQuery(name = "Businesshours.findByStartTime", query = "SELECT b FROM Businesshours b WHERE b.startTime = :startTime")
    , @NamedQuery(name = "Businesshours.findByEndTime", query = "SELECT b FROM Businesshours b WHERE b.endTime = :endTime")
    , @NamedQuery(name = "Businesshours.findByCreatedDateTime", query = "SELECT b FROM Businesshours b WHERE b.createdDateTime = :createdDateTime")
    , @NamedQuery(name = "Businesshours.findByCreatedUserId", query = "SELECT b FROM Businesshours b WHERE b.createdUserId = :createdUserId")
    , @NamedQuery(name = "Businesshours.findByUpdatedDateTime", query = "SELECT b FROM Businesshours b WHERE b.updatedDateTime = :updatedDateTime")
    , @NamedQuery(name = "Businesshours.findByUpdatedUserId", query = "SELECT b FROM Businesshours b WHERE b.updatedUserId = :updatedUserId")})
public class Businesshours implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "businessHoursId")
    private Integer businessHoursId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dayType")
    private int dayType;
    @Size(max = 20)
    @Column(name = "startTime")
    private String startTime;
    @Size(max = 20)
    @Column(name = "endTime")
    private String endTime;
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;
    @Size(max = 20)
    @Column(name = "createdUserId")
    private String createdUserId;
    @Column(name = "updatedDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDateTime;
    @Size(max = 20)
    @Column(name = "updatedUserId")
    private String updatedUserId;

    public Businesshours() {
    }

    public Businesshours(Integer businessHoursId) {
        this.businessHoursId = businessHoursId;
    }

    public Businesshours(Integer businessHoursId, int dayType) {
        this.businessHoursId = businessHoursId;
        this.dayType = dayType;
    }

    public Integer getBusinessHoursId() {
        return businessHoursId;
    }

    public void setBusinessHoursId(Integer businessHoursId) {
        this.businessHoursId = businessHoursId;
    }

    public int getDayType() {
        return dayType;
    }

    public void setDayType(int dayType) {
        this.dayType = dayType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (businessHoursId != null ? businessHoursId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Businesshours)) {
            return false;
        }
        Businesshours other = (Businesshours) object;
        if ((this.businessHoursId == null && other.businessHoursId != null) || (this.businessHoursId != null && !this.businessHoursId.equals(other.businessHoursId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.Businesshours[ businessHoursId=" + businessHoursId + " ]";
    }
    
}
