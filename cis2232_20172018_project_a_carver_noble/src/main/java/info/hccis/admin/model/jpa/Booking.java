/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dcarver
 */
@Entity
@Table(name = "booking")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Booking.findAll", query = "SELECT b FROM Booking b")
//    , @NamedQuery(name = "Booking.findByBookingId", query = "SELECT b FROM Booking b WHERE b.bookingId = :bookingId")
//    , @NamedQuery(name = "Booking.findByUserId", query = "SELECT b FROM Booking b WHERE b.userId = :userId")
//    , @NamedQuery(name = "Booking.findByCourtId", query = "SELECT b FROM Booking b WHERE b.courtId = :courtId")
//    , @NamedQuery(name = "Booking.findByOpponent", query = "SELECT b FROM Booking b WHERE b.opponent = :opponent")
//    , @NamedQuery(name = "Booking.findByBookingDate", query = "SELECT b FROM Booking b WHERE b.bookingDate = :bookingDate")
//    , @NamedQuery(name = "Booking.findByBookingStartTime", query = "SELECT b FROM Booking b WHERE b.bookingStartTime = :bookingStartTime")
//    , @NamedQuery(name = "Booking.findByCreatedDateTime", query = "SELECT b FROM Booking b WHERE b.createdDateTime = :createdDateTime")
//    , @NamedQuery(name = "Booking.findByCreatedUserId", query = "SELECT b FROM Booking b WHERE b.createdUserId = :createdUserId")
//    , @NamedQuery(name = "Booking.findByUpdatedDateTime", query = "SELECT b FROM Booking b WHERE b.updatedDateTime = :updatedDateTime")
//    , @NamedQuery(name = "Booking.findByUpdatedUserId", query = "SELECT b FROM Booking b WHERE b.updatedUserId = :updatedUserId")})
public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bookingId")
    private Integer bookingId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "userId")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "courtId")
    private int courtId;
    @Column(name = "opponent")
    private Integer opponent;
    @Size(max = 20)
    @Column(name = "bookingDate")
    private String bookingDate;
    @Size(max = 20)
    @Column(name = "bookingStartTime")
    private String bookingStartTime;
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;
    @Size(max = 20)
    @Column(name = "createdUserId")
    private String createdUserId;
    @Column(name = "updatedDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDateTime;
    @Size(max = 20)
    @Column(name = "updatedUserId")
    private String updatedUserId;
    @Transient
    private int courtType;

    public int getCourtType() {
        return courtType;
    }

    public void setCourtType(int courtType) {
        this.courtType = courtType;
    }
    
    public Booking() {
    }

    public Booking(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Booking(Integer bookingId, int userId, int courtId) {
        this.bookingId = bookingId;
        this.userId = userId;
        this.courtId = courtId;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCourtId() {
        return courtId;
    }

    public void setCourtId(int courtId) {
        this.courtId = courtId;
    }

    public Integer getOpponent() {
        return opponent;
    }

    public void setOpponent(Integer opponent) {
        this.opponent = opponent;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingStartTime() {
        return bookingStartTime;
    }

    public void setBookingStartTime(String bookingStartTime) {
        this.bookingStartTime = bookingStartTime;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookingId != null ? bookingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Booking)) {
            return false;
        }
        Booking other = (Booking) object;
        if ((this.bookingId == null && other.bookingId != null) || (this.bookingId != null && !this.bookingId.equals(other.bookingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.jpa.Booking[ bookingId=" + bookingId + " ]";
    }
    
}
